import React, {Component} from 'react';
import {Route, Router, Switch} from 'react-router-dom';
import './App.css';

import Page1 from "./components/Page1/Page1";
import Page2 from "./components/Page2/Page2";
import Page3 from "./components/Page3/Page3";
import HomePage from "./components/HomePage/HomePage";
import Navigation from "./components/Header/Navigation/Navigation";
import Footer from "./components/Footer/Footer";
import NotFound from "./components/Error/NotFound/NotFound";


class App extends Component {
    render() {
        return (
            <div>
                <Navigation/>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/page1" component={Page1}/>
                    <Route path="/page2" component={Page2}/>
                    <Route path="/page3" component={Page3}/>
                    <Route render={() => <NotFound style={{textAlign: 'center'}} component={NotFound}/>}/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}

export default App;
