import React from 'react';
import {NavLink} from 'react-router-dom';
import './Navigation.css';

const Navigation = props => {
    return (
        <div className="Navigation">
            {/*<div className="NavLogo"></div>*/}
            <a href="//marvel.com/" class="icon-marvel-logo-svg" title="home" alt="home" className="NavLogo"></a>
            <ul>
                <li>
                    <NavLink to="/" activeClassName="selected">HomePage</NavLink>
                </li>
                <li>
                    <NavLink to="/page1" activeClassName="selected">Page1</NavLink>
                </li>
                <li>
                    <NavLink to="/page2" activeClassName="selected">Page2</NavLink>
                </li>
                <li>
                    <NavLink to="/page3" activeClassName="selected">Page3</NavLink>
                </li>
                <li>
                    <NavLink to="/page5" activeClassName="selected">Not Found</NavLink>
                </li>
            </ul>
        </div>
    )
};

export default Navigation;