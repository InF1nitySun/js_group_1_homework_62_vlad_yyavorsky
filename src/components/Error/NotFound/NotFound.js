import React from 'react';
import './NotFound.css'

const NotFound = props => {
    return (
        <div className="NotFound Container">
            <h1>Error 404: not found</h1>
            <p className="SubTitle">Program missing, exiting protocol...</p>
            <p className="SecondSub">Make sure you typed in the page address correctly or go back to your previous page.</p>
            <img className="Ironman" src="//i.annihil.us/u/prod/marvel/html_blocks_assets/ironman404/ironman.png"/>
        </div>
)
};

export default NotFound;