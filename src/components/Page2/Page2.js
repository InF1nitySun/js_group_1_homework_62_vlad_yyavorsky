import React from 'react';

const Page2 = props => {
    return (
        <div className="Page2 Container">
            <div>
                <h2>Чёрная пантера (2018)</h2>
                <p>Основная статья: <a
                    href="https://ru.wikipedia.org/wiki/%D0%A7%D1%91%D1%80%D0%BD%D0%B0%D1%8F_%D0%9F%D0%B0%D0%BD%D1%82%D0%B5%D1%80%D0%B0_(%D1%84%D0%B8%D0%BB%D1%8C%D0%BC)"
                    title="Чёрная Пантера (фильм)">Чёрная Пантера (фильм)</a></p>
                <p>
                    В 2011 году Марк Бэйли был утверждён на пост сценариста фильма о Чёрной пантере.
                    В октябре 2014 года был объявлен исполнитель главной роли — Чедвик Боузман, который
                    исполнил эту роль ещё и в третьем фильме о Капитане Америка. Релиз фильма состоялся 16 февраля
                    2018 года.
                </p>
                <div className="Media">
                    <div className="Video" style={{ width: '560px', height:'315px'}}>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/eFPWKXF2oRo" frameborder="0"
                                allow="autoplay; encrypted-media" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>

        </div>
    )
};

export default Page2;