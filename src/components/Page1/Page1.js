import React from 'react';

const Page1 = props => {
    return (
        <div className="Page1 Container">
            <div>
                <h2>Тор: Рагнарёк (2017)</h2>
                <p>Основная статья: <a
                    href="https://ru.wikipedia.org/wiki/%D0%A2%D0%BE%D1%80:_%D0%A0%D0%B0%D0%B3%D0%BD%D0%B0%D1%80%D1%91%D0%BA"
                    title="Тор: Рагнарёк">Тор: Рагнарёк</a></p>
                <p>В январе 2014 года были утверждены сценаристы на третью часть «Тора»:
                    Кристофер Йост и Крэйг Кайл. Крис Хемсворт и Том Хиддлстон в очередной раз
                    исполнили роли сводных братьев Тора и Локи. В фильме также появился Халк в исполнении
                    Марка Руффало. Официальная дата релиза — 3 ноября 2017 года.
                </p>
                <div className="Media">
                    <div className="Video" style={{ width: '560px', height:'315px'}}>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/8Idj0Ky8O6U" frameborder="0"
                                allow="autoplay; encrypted-media" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>

        </div>
    )
};

export default Page1;