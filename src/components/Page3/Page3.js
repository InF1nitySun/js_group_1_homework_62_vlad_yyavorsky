import React from 'react';

const Page3 = props => {
    return (
        <div className="Page3 Container">
            <div>
                <h2>Мстители: Война бесконечности (2018)</h2>
                <p>Основная статья: <a
                    href="https://ru.wikipedia.org/wiki/%D0%9C%D1%81%D1%82%D0%B8%D1%82%D0%B5%D0%BB%D0%B8:_%D0%92%D0%BE%D0%B9%D0%BD%D0%B0_%D0%B1%D0%B5%D1%81%D0%BA%D0%BE%D0%BD%D0%B5%D1%87%D0%BD%D0%BE%D1%81%D1%82%D0%B8"
                    title="Мстители: Война бесконечности">Мстители: Война бесконечности</a></p>
                <p>Режиссёрами выступят Энтони и Джо Руссо, ранее работавшие над вторым и третьим
                    сегментом трилогии о Капитане Америка. Марк Руффало и Том Хиддлстон вернутся к ролям Халка
                    и Локи в этой и последующей части «Войны Бесконечности». Дата выхода — 4 мая 2018
                    года.</p>
                <div className="Media">
                    <div className="Video" style={{ width: '560px', height:'315px'}}>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/FeJKZMFJ7NA" frameborder="0"
                                allow="autoplay; encrypted-media" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>

        </div>
    )
};

export default Page3;