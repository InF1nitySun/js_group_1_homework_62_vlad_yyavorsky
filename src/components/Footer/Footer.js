import React from 'react';
import NavLink from "react-router-dom/es/NavLink";
import './Footer.css';


const Footer = props => {
    return (
        <div className="Footer">
            <ul>
                <li>
                    <NavLink to="/" activeClassName="selected">HomePage</NavLink>
                </li>
                <li>
                    <NavLink to="/page1" activeClassName="selected">Page1</NavLink>
                </li>
                <li>
                    <NavLink to="/page2" activeClassName="selected">Page2</NavLink>
                </li>
                <li>
                    <NavLink to="/page3" activeClassName="selected">Page3</NavLink>
                </li>
                <li>
                    <NavLink to="/page5" activeClassName="selected">Not Found</NavLink>
                </li>
            </ul>
        </div>
    )
};

export default Footer;